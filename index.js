import JSZip from "jszip";
import csv from "csvtojson";
import tinycolor from "tinycolor2";

import manualMetadata from "./manualMetadata.json";

const jz = new JSZip();

addEventListener("fetch", event => {
    event.respondWith(handleRequest(event.request))
});

addEventListener("scheduled", event => {
    event.waitUntil(handleSchedule())
});

// Reads a CSV file from JSZip into JSON
async function readZipFileToJSON(zip, name) {
    const text = await zip.file(name).async("string");
    const json = await csv().fromString(text);
    return json;
}

// Gets the timetable name (e.g. August 30, 2021) from GTFS feed info
function timetableNameFromGTFS(feedInfo) {
    const gtfsDate = feedInfo[0].feed_start_date;
    const date = new Date(gtfsDate.substring(0, 4) + "-" + gtfsDate.substring(4, 6) + "-" + gtfsDate.substring(6, 8));
    return `${date.toLocaleDateString("en-US", { timeZone: 'UTC', month: "long" })} ${date.getDate()}, ${date.getFullYear()}`;
}

function goodStopName(badName) {
    return badName
        .replace("Station", "")
        .replace(/[^a-zA-Z\d &/]/, "")
        .trim();
}

// Gets the good (and unique) stop name used to merge stop IDs
function findSuperStopName(badName) {
    let normBadName = goodStopName(badName)
        .toUpperCase()
        .replace("AVENUE", "AVE")
        .replace("DIRIDON", "")
        .replace("SAN JOSE", "SAN JOSE DIRIDON");
    let bestName = "";
    for (const goodName of manualMetadata.stopNames) {
        if (normBadName.includes(goodName.toUpperCase()) && (goodName.length > bestName.length)) bestName = goodName;
    }
    if (!bestName) {
        throw new Error("Failed to find a good stop name for stop " + badName);
    }
    return bestName;
}

async function handleRequest(request) {
    const result = await handleSchedule(new URL(request.url).searchParams.get("force") === "true", new URL(request.url).searchParams.get("dryRun") !== "false");
    return new Response(result, { status: 200, headers: { "Content-Type": "application/json", "X-Robots-Tag": "noindex" } });
}

// https://stackoverflow.com/questions/18883601/function-to-calculate-distance-between-two-coordinates
// This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
const R = 6371; // km
function calcDistance(pt1, pt2) {
    const dLat = toRad(pt2.lat - pt1.lat);
    const dLon = toRad(pt2.lon - pt1.lon);
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(toRad(pt1.lat)) * Math.cos(toRad(pt2.lat));
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return R * c; // Distance in km
}

// Converts numeric degrees to radians
function toRad(Value) {
    return Value * Math.PI / 180;
}

function distanceAlongShape(pt, shape) {
    let bestSegmentStartIndex = 0, bestDiff = Infinity;
    for (let i = 0; i < shape.length - 1; i++) {
        const segmentStart = shape[i], segmentEnd = shape[i + 1];
        const directLength = calcDistance(segmentStart, segmentEnd);
        const lengthThroughPt = calcDistance(segmentStart, pt) + calcDistance(pt, segmentEnd);
        const diff = lengthThroughPt - directLength;
        if (diff < 0) throw new Error("Invariant violation: It should not be possible for lengthThroughPt to be less than directLength");
        if (diff < bestDiff) {
            bestDiff = diff;
            bestSegmentStartIndex = i;
        }
    }
    let cumulativeDistance = 0;
    for (let i = 0; i <= bestSegmentStartIndex; i++) {
        const segmentStart = shape[i], segmentEnd = i === bestSegmentStartIndex ? pt : shape[i + 1];
        cumulativeDistance += calcDistance(segmentStart, segmentEnd);
    }
    return cumulativeDistance;
}

async function handleSchedule(force = false, dryRun = false) {
    console.log("Updating timetable");

    const agency = "CT";
    const CT = agency === "CT";

    const gtfsRes = await fetch("https://api.511.org/transit/datafeeds?operator_id=CT&api_key=" + API_KEY + "&status=active");
    const gtfsBlob = await gtfsRes.arrayBuffer();
    const zip = await jz.loadAsync(gtfsBlob);

    console.log("Got data");

    const existingLiveasset = await TIMETABLE.get("liveasset");
    const feedInfo = await readZipFileToJSON(zip, "feed_info.txt");

    console.log("Opened zip");

    if ((force !== true) && existingLiveasset && (JSON.parse(existingLiveasset).id === feedInfo[0].feed_version)) return "No update";

    const [
        stops,
        trips,
        stopTimes,
        calendar,
        routes,
        calendarDates,
        shapes
    ] = await Promise.all([
        readZipFileToJSON(zip, "stops.txt"),
        readZipFileToJSON(zip, "trips.txt"),
        readZipFileToJSON(zip, "stop_times.txt"),
        readZipFileToJSON(zip, "calendar.txt"),
        readZipFileToJSON(zip, "routes.txt"),
        readZipFileToJSON(zip, "calendar_dates.txt"),
        readZipFileToJSON(zip, "shapes.txt")
    ]);

    console.log("Extracted assets");

    const liveasset = {
        liveasset: {
            agency,
            name: timetableNameFromGTFS(feedInfo),
            ver: new Date().getTime(),
            id: feedInfo[0].feed_version,
            type: "backend",
            ua: "AliceSprings/8.0",
            expiry: feedInfo[0].feed_end_date || null
        },
        trips: {},
        tracks: {},
        service: {
            regular: {
                days: { 0: [], 1: [], 2: [], 3: [], 4: [], 5: [], 6: [] },
                ranges: {},
                lineColors: {
                    lineType: {},
                    lineUI: {},
                    lineUIText: {}
                },
                lineNames: {}
            },
            dates: []
        }
    };

    console.log("Getting lines");

    // Get lines
    const tracks = CT ? ["super"] : [];
    for (const route of routes) {
        // TODO: This breaks the Limited line, because for some reason, its route_type is 3 (bus) as of Nov 14, 2024
        // if (route.route_type !== "2") continue;

        var hsl = tinycolor("#" + route.route_color).toHsl();

        liveasset.service.regular.lineColors.lineType[route.route_id] = {
            light: tinycolor({ ...hsl, l: 0.36 }).toHexString(),
            dark: tinycolor({ ...hsl, l: 0.82 }).toHexString()
        };
        liveasset.service.regular.lineColors.lineUI[route.route_id] = {
            light: tinycolor({ ...hsl, l: 0.36 }).toHexString(),
            dark: tinycolor({ ...hsl, l: 0.76 }).toHexString()
        };
        liveasset.service.regular.lineColors.lineUIText[route.route_id] = {
            light: tinycolor({ ...hsl, l: 0.96 }).toHexString(),
            dark: tinycolor({ ...hsl, l: 0.04 }).toHexString()
        };

        // Sometimes only the long or short name is provided
        liveasset.service.regular.lineNames[route.route_id] = {
            short: route.route_short_name || route.route_long_name,
            long: route.route_long_name || route.route_short_name
        };

        if (!CT) tracks.push(route.route_id);
    }

    console.log("Parsing trains");

    // Parse each train
    for (const trip of trips) {
        if (!liveasset.service.regular.lineNames[trip.route_id]) continue;
        const number = trip.trip_short_name;

        liveasset.trips[trip.trip_id] = {
            name: number || null,
            id: trip.trip_id, // full trip ID
            line: trip.route_id,
            service: trip.service_id,
            track: CT ? "super" : trip.route_id,
            stops: [],
            ...(CT ? (manualMetadata.patch.trains[number] ?? {}) : {})
        };
    }

    console.log("Adding stop times to trains");

    // Add stops to train
    for (const stopTime of stopTimes) {
        if (!liveasset.trips[stopTime.trip_id]) continue;
        liveasset.trips[stopTime.trip_id].stops.push({
            id: stopTime.stop_id,
            timetable: {
                dep: stopTime.departure_time,
                arr: stopTime.arrival_time
            }
        });
    }

    console.log("Calculating tracks...");

    for (const trackID of tracks) {
        liveasset.tracks[trackID] = [];
        const track = liveasset.tracks[trackID];

        console.log(`[${trackID}]`, "Analyzing shapes...");

        let shapeID;
        if (CT) {
            let longestShape = null;
            for (const point of shapes) {
                if (!longestShape || (Number(point.shape_dist_traveled) > longestShape.length)) longestShape = { id: point.shape_id, length: Number(point.shape_dist_traveled) };
            }
            shapeID = longestShape.id;
        } else {
            const allowedShapes = [];
            for (const trip of trips) {
                if ((trip.route_id === trackID) && !allowedShapes.includes(trip.shape_id)) allowedShapes.push(trip.shape_id);
            }
            let longestShape = null;
            for (const point of shapes) {
                if (!allowedShapes.includes(point.shape_id)) continue;
                if (!longestShape || (Number(point.shape_dist_traveled) > longestShape.length)) longestShape = { id: point.shape_id, length: Number(point.shape_dist_traveled) };
            }
            shapeID = longestShape.id;
        }

        const shape = shapes.filter(point => point.shape_id === shapeID).map(pt => ({
            ...pt,
            lat: Number(pt.shape_pt_lat),
            lon: Number(pt.shape_pt_lon),
            shape_pt_sequence: Number(pt.shape_pt_sequence),
            shape_dist_traveled: pt.shape_dist_traveled ? Number(pt.shape_dist_traveled) : null
        })).sort((a, b) => a.shape_pt_sequence - b.shape_pt_sequence);
        if (shape[0].lat < shape[shape.length - 1].lat) shape.reverse(); // north stop top stop

        console.log(`[${trackID}]`, "Parsing stops...");

        // Parse stops - Pass 1 (parent stops)
        const allStops = [];
        for (const stop of stops) {
            if (stop.parent_station) continue;
            const bestName = CT ? findSuperStopName(stop.stop_name) : goodStopName(stop.stop_name);
            const existingStop = allStops.find(s => (s.name === bestName));

            if (!existingStop) {
                const location = { lat: Number(stop.stop_lat), lon: Number(stop.stop_lon) };
                const IDs = [stop.stop_id];
                const s = {
                    IDs,
                    name: bestName,
                    aliases: [],
                    distance: distanceAlongShape(location, shape),
                    ...(CT ? (manualMetadata.patch.stops[bestName] ?? {}) : {})
                };
                if ((bestName !== stop.stop_name) && !s.aliases.includes(stop.stop_name)) s.aliases.push(stop.stop_name);

                allStops.push(s);
            } else {
                if (!existingStop.IDs.includes(stop.stop_id)) existingStop.IDs.push(stop.stop_id);
                if ((existingStop.name !== stop.stop_name) && !existingStop.aliases.includes(stop.stop_name)) existingStop.aliases.push(stop.stop_name);
            }
        }

        // Parse stops - Pass 2 (child stops)
        for (const stop of stops) {
            if (!stop.parent_station) continue;

            const existingStop = allStops.find(s => s.IDs.includes(stop.parent_station));
            if (!existingStop) throw new Error("Cannot find parent stop for stop ID " + stop.stop_id);

            if (!existingStop.IDs.includes(stop.stop_id)) existingStop.IDs.push(stop.stop_id);
            if ((existingStop.name !== stop.stop_name) && !existingStop.aliases.includes(stop.stop_name)) existingStop.aliases.push(stop.stop_name);
        }

        console.log(`[${trackID}]`, "Filtering stops");
        for (const stop of allStops) {
            for (const tripID in liveasset.trips) {
                if ((liveasset.trips[tripID].track === trackID) && liveasset.trips[tripID].stops.find(s => stop.IDs.includes(s.id))) {
                    if (!track.includes(stop)) track.push(stop);
                    break;
                }
            }
        }

        console.log(`[${trackID}]`, "Sorting stop segments");

        //Sort & calculate stop segment distances
        track.sort((a, b) => a.distance - b.distance);
        track[0].distancePrev = track[0].distance;
        for (let i = 1; i < track.length; i++) {
            track[i].distancePrev = track[i].distance - track[i - 1].distance;
            if (track[i].distancePrev < 0) throw new Error("Invariant violation: Cannot have a negative distance between stops along a route");
        }

        console.log(`[${trackID}]`, "Computing trip directions");
        for (const tripID in liveasset.trips) {
            if (liveasset.trips[tripID].track !== trackID) continue;
            const trip = liveasset.trips[tripID];
            const trackDelta = track.findIndex(s => s.IDs.includes(trip.stops[trip.stops.length - 1].id)) - track.findIndex(s => s.IDs.includes(trip.stops[0].id));
            trip.dir = trackDelta < 0 ? "NB" : "SB";
        }
    }

    console.log("Calculating stop bypasses");

    // Calculate stop bypasses
    for (const train of Object.values(liveasset.trips)) {
        train.stops.forEach((stop, i) => {
            if (i === 0) return;

            // Get stop IDs and indices
            const prevStop = train.stops[i - 1];
            const currentIndex = liveasset.tracks[train.track].findIndex(s => s.IDs.includes(stop.id)), prevIndex = liveasset.tracks[train.track].findIndex(s => s.IDs.includes(prevStop.id));

            // Skip if the stops are directly adjacent
            if (Math.abs(currentIndex - prevIndex) === 1) return;

            // Create function for looping through stops and injecting bypasses depending on direction
            let next, check;
            if (currentIndex > prevIndex) {
                next = num => num + 1;
                check = num => num < currentIndex;
            } else if (currentIndex < prevIndex) {
                next = num => num - 1;
                check = num => num > currentIndex;
            }

            // Loop through stops to find bypasses
            const bypassedStops = [];
            for (var iii = next(prevIndex); check(iii); iii = next(iii)) {
                // Store bypasses
                bypassedStops.push(iii);
            }

            bypassedStops.forEach(stopIndex => {
                const station = liveasset.tracks[train.track][stopIndex];

                // calculate percentage of previous station between this stop and last stop
                // | bypassed dist - prev dist | / | current dist - prev dist | 
                const percentage = Math.abs(station.distance - liveasset.tracks[train.track][prevIndex].distance) / Math.abs(liveasset.tracks[train.track][currentIndex].distance - liveasset.tracks[train.track][prevIndex].distance);

                // Percentage used in app (even in scheduled)
                // Formula: prevTime + (timeBetween * percentage) [timeBetween and prevTime use realtime data]

                // Store bypass
                if (!stop.bypassed) stop.bypassed = [];
                stop.bypassed.push({
                    stop: stopIndex,
                    percentage: percentage
                });
            });
        });
    }

    console.log("Scheduling trains");

    // Get service data for each day of the week
    for (const dow of calendar) {
        let serviceInUse = false;
        for (const tripID in liveasset.trips) {
            if (liveasset.trips[tripID].service === dow.service_id) {
                serviceInUse = true;
                break;
            }
        }
        if (!serviceInUse) continue;

        // Add regular service days
        if (dow.sunday === "1") liveasset.service.regular.days[0].push(dow.service_id);
        if (dow.monday === "1") liveasset.service.regular.days[1].push(dow.service_id);
        if (dow.tuesday === "1") liveasset.service.regular.days[2].push(dow.service_id);
        if (dow.wednesday === "1") liveasset.service.regular.days[3].push(dow.service_id);
        if (dow.thursday === "1") liveasset.service.regular.days[4].push(dow.service_id);
        if (dow.friday === "1") liveasset.service.regular.days[5].push(dow.service_id);
        if (dow.saturday === "1") liveasset.service.regular.days[6].push(dow.service_id);

        // Add service date ranges
        liveasset.service.regular.ranges[dow.service_id] = {
            start: dow.start_date,
            end: dow.end_date
        };
    }

    // Get service exceptions
    for (const date of calendarDates) {
        let serviceInUse = false;
        for (const tripID in liveasset.trips) {
            if (liveasset.trips[tripID].service === date.service_id) {
                serviceInUse = true;
                break;
            }
        }
        if (!serviceInUse) continue;

        // Add exception to calendar
        liveasset.service.dates.push(date);
    }

    if (dryRun) {
        console.log("Dry run; not saving anything...");
    } else {
        console.log("Saving & sending update notification...");
        await Promise.all([
            TIMETABLE.put("timetable", JSON.stringify(liveasset)),
            TIMETABLE.put("liveasset", JSON.stringify(liveasset.liveasset)),
            OPS_ALERTS.fetch("http://ops-alerts.ns.jottocraft.com/dispatch", {
                method: "POST",
                body: JSON.stringify({
                    origin: "train-assets",
                    title: "Automatic .liveasset update deployed",
                    msg: [
                        "The AliceSprings .liveasset worker has deployed an automatic timetable update.",
                        "",
                        "name: " + liveasset.liveasset.name,
                        "id:   " + liveasset.liveasset.id,
                        "ver:  " + liveasset.liveasset.ver,
                        "ua:   " + liveasset.liveasset.ua
                    ].join("\n"),
                    product: "AliceSprings",
                    severity: "WRN"
                }),
                headers: {
                    "Content-Type": "application/json"
                }
            })
        ]);
    }

    console.log("Done :)");

    return JSON.stringify(liveasset);
}
