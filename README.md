![Caltrain Live logo](https://i.imgur.com/T0PHvch.png)

This repository contains the source code for the Caltrain Live Realtime timetable generator. Its purpose is to periodically check the latest timetable published on [511](https://511.org) and, if a new version is available, generate a `.caltrainasset` JSON file that can be served to and parsed by the [Caltrain Live mobile app](https://gitlab.com/jottocraft/train/app). This code runs on the [Cloudflare Workers](https://workers.cloudflare.com/) platform as a cron job every 30 minutes.

### Resources

- [Source code](https://gitlab.com/jottocraft/train)
- [Main website](https://train.jottocraft.com)
- [App on Google Play](https://play.google.com/store/apps/details?id=com.jottocraft.train)